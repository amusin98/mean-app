Prerequirements: 
 - JDK 1.8
 - Gradle
 - Node(^8.11.2)
 - npm(^5.6.0)

Run:
    Backend:
      Java:
        - cd backend/java
        - gradlew bootRun
      
      Nodejs:
        - cd backend/nodejs
        - npm i
        - npm start or nodemon
      
    Frontend: 
    
    Configure backend-type: webpack.config.js => dev-server=> proxy => port(8090 => java, 9090 => nodejs)
    
    - cd frontend
    - npm i
    - npm start(or npm run mockserv w/o backend)
      