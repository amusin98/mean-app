package by.trees.trees_api.controller;

import by.trees.trees_api.model.Document;
import by.trees.trees_api.service.DocumentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/documents")
public class DocumentsController {

    @Autowired
    private DocumentsService service;

    @GetMapping
    public ResponseEntity<List<Document>> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Document> create(@RequestBody Document node) {
        Optional<Document> newNode = service.insert(node);
        return newNode.map(document -> new ResponseEntity<>(document, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PutMapping(value = "/{id}/meta")
    public ResponseEntity<Document> updateMeta(@PathVariable String id, @RequestBody Map metadata) {
        Optional<Document> updatedNode = service.updateMeta(id, metadata);
        return updatedNode.map(document -> new ResponseEntity<>(document, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Document> update(@PathVariable String id, @RequestBody Document node) {
        Optional<Document> updatedNode = service.update(id, node);
        return updatedNode.map(document -> new ResponseEntity<>(document, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        boolean result = service.delete(id);
        if (result) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
