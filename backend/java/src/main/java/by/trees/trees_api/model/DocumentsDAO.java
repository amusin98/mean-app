package by.trees.trees_api.model;

import by.trees.trees_api.model.mapper.DocumentMapper;
import com.mongodb.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class DocumentsDAO {

    @Autowired
    private DocumentMapper mapper;

    @Autowired
    private DBCollection documents;

    public List<Document> getAll() {
        List<Document> documentsResponse = new ArrayList<>();
        DBCursor dbObjects = documents.find();
        dbObjects.forEach(dbObject -> documentsResponse.add(mapper.map(dbObject)));
        dbObjects.close();
        return documentsResponse;
    }

    public Document getById(String id) {
        return mapper.map(documents.findOne(new BasicDBObject("_id", id)));
    }

    public boolean updateMeta(String id, Map meta) {
        DBObject object = documents.findOne(new BasicDBObject("_id", id));
        object.putAll(meta);
        WriteResult save = documents.save(object);
        return save.getN() == 1;
    }

    public boolean delete(String id) {
        WriteResult result = documents.remove(new BasicDBObject("_id", id));
        if (result.getN() != 1) {
            return false;
        }
        DBCursor childs = documents.find(new BasicDBObject().append("parent", id));
        for (DBObject child : childs) {
            if (!delete((String) child.get("_id"))) {
                return false;
            }
        }
        childs.close();
        return true;
    }

    public boolean update(String id, Document document) {
        WriteResult result = documents.remove(new BasicDBObject("_id", id));
        if (result.getN() != 1) {
            return false;
        }
        if(!insert(document)) {
            return false;
        }
        DBCursor childs = documents.find(new BasicDBObject().append("parent", id));
        for (DBObject child : childs) {
            child.put("parent", document.get_id());
            WriteResult update = documents.update(new BasicDBObject("_id", child.get("_id")), child);
            if (update.getN() != 1) {
                return false;
            }
        }
        childs.close();
        return true;
    }


    public boolean insert(Document document) {
        WriteResult save = documents.save(mapper.map(document));
        return save.getN() == 1;
    }

}
