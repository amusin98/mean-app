package by.trees.trees_api.model.mapper;

import by.trees.trees_api.model.Document;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DocumentMapper {
    public Document map(DBObject dbObject) {
        return new Document((String) dbObject.get("_id"), (String) dbObject.get("parent"), (Map) dbObject.get("metadata"));
    }

    public DBObject map(Document document) {
        return new BasicDBObject("_id", document.get_id())
                .append("parent", document.getParent())
                .append("metadata", document.getMetadata());
    }
}
