package by.trees.trees_api.service;

import by.trees.trees_api.model.Document;
import by.trees.trees_api.model.DocumentsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class DocumentsService {

    @Autowired
    private DocumentsDAO dao;

    public Optional<Document> insert(Document document) {
        boolean result = dao.insert(document);
        return result ? Optional.of(document) : Optional.empty();
    }

    public boolean delete(String id) {
        return dao.delete(id);
    }

    public List<Document> getAll() {
        return dao.getAll();
    }

    public Optional<Document> update(String id, Document document) {
        boolean result = dao.update(id, document);
        return result ? Optional.of(document) : Optional.empty();
    }

    public Optional<Document> updateMeta(String id, Map metadata) {
        boolean result = dao.updateMeta(id, metadata);
        return result ? Optional.of(dao.getById(id)) : Optional.empty();
    }
}
