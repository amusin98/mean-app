package by.trees.trees_api.controller;

import by.trees.trees_api.model.Document;
import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpStatus;
import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

public class DocumentControllerTestData {
    @DataProvider(name = "getAll")
    public static Object[][] getAllData() {
        return new Object[][] {
                {Arrays.asList(
                        new Document("node1", "parent1", Collections.emptyMap()),
                        new Document("node2", "parent2", Collections.emptyMap())),
                        HttpStatus.OK},
                {Collections.emptyList(), HttpStatus.OK}
        };
    }

    @DataProvider(name = "insert")
    public static Object[][] insertData() {
        Document mock = new Document("node1", "parent1", Collections.emptyMap());
        return new Object[][] {
                {mock, Optional.of(mock), HttpStatus.CREATED},
                {mock, Optional.empty(), HttpStatus.BAD_REQUEST}
        };
    }

    @DataProvider(name = "update")
    public static Object[][] updateData() {
        Document mock = new Document("node1", "parent1", Collections.emptyMap());
        return new Object[][] {
                {"oldNodeId", mock, Optional.of(mock), HttpStatus.CREATED},
                {"oldNodeId", mock, Optional.empty(), HttpStatus.BAD_REQUEST}
        };
    }

    @DataProvider(name = "updateMeta")
    public static Object[][] updateMetadataData() {
        Document mock = new Document("node1", "parent1", Collections.emptyMap());
        return new Object[][] {
                {"oldNodeId", ImmutableMap.of("name", "value"), Optional.of(mock), HttpStatus.CREATED},
                {"oldNodeId", ImmutableMap.of("name", "value"), Optional.empty(), HttpStatus.BAD_REQUEST}
        };
    }

    @DataProvider(name = "delete")
    public static Object[][] deleteData() {
        return new Object[][] {
                {"nodeId", true, HttpStatus.NO_CONTENT},
                {"nodeId", false, HttpStatus.BAD_REQUEST}
        };
    }
}
