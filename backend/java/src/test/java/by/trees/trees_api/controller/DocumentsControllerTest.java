package by.trees.trees_api.controller;

import by.trees.trees_api.TreesApiApplication;
import by.trees.trees_api.model.Document;
import by.trees.trees_api.service.DocumentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestExecutionListeners(MockitoTestExecutionListener.class)
@WebMvcTest(DocumentsController.class)
@ContextConfiguration(classes = TreesApiApplication.class)
public class DocumentsControllerTest extends AbstractTestNGSpringContextTests {


    @Autowired
    MockMvc mockMvc;

    @MockBean
    private DocumentsService service;


    @Test(dataProvider = "getAll", dataProviderClass = DocumentControllerTestData.class)
    public void testGetAll(List<Document> response, HttpStatus status) throws Exception {
        given(service.getAll()).willReturn(response);
        mockMvc.perform(get("/api/documents"))
                .andExpect(status().is(status.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(response.size())));


    }

    @Test(dataProvider = "insert", dataProviderClass = DocumentControllerTestData.class)
    public void testCreate(Document document, Optional<Document> response, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(document);
        given(service.insert(document)).willReturn(response);
        if (response.isPresent()) {
            mockMvc.perform(post("/api/documents")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$._id", is(response.get().get_id())))
                    .andExpect(jsonPath("$.parent", is(response.get().getParent())))
                    .andExpect(jsonPath("$.metadata", is(response.get().getMetadata())));
        } else {
            mockMvc.perform(post("/api/documents")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()));
        }
    }

    @Test(dataProvider = "updateMeta", dataProviderClass = DocumentControllerTestData.class)
    public void testUpdateMeta(String id, Map metadata, Optional<Document> response, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(metadata);
        given(service.updateMeta(id, metadata)).willReturn(response);
        if (response.isPresent()) {
            mockMvc.perform(put("/api/documents/{id}/meta", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$._id", is(response.get().get_id())))
                    .andExpect(jsonPath("$.parent", is(response.get().getParent())))
                    .andExpect(jsonPath("$.metadata", is(response.get().getMetadata())));
        } else {
            mockMvc.perform(put("/api/documents/{id}/meta", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()));
        }
    }

    @Test(dataProvider = "update", dataProviderClass = DocumentControllerTestData.class)
    public void testUpdate(String id, Document document, Optional<Document> response, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(document);
        given(service.update(id, document)).willReturn(response);
        if (response.isPresent()) {
            mockMvc.perform(put("/api/documents/{id}", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$._id", is(response.get().get_id())))
                    .andExpect(jsonPath("$.parent", is(response.get().getParent())))
                    .andExpect(jsonPath("$.metadata", is(response.get().getMetadata())));
        } else {
            mockMvc.perform(put("/api/documents/{id}", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()));
        }
    }

    @Test(dataProvider = "delete", dataProviderClass = DocumentControllerTestData.class)
    public void testDelete(String id, boolean result, HttpStatus status) throws Exception {
        given(service.delete(id)).willReturn(result);
        mockMvc.perform(delete("/api/documents/{id}", id))
                .andExpect(status().is(status.value()));
    }
}