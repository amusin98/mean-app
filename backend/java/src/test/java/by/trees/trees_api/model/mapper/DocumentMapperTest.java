package by.trees.trees_api.model.mapper;

import by.trees.trees_api.TreesApiApplication;
import by.trees.trees_api.model.Document;
import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(classes = TreesApiApplication.class)
public class DocumentMapperTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private DocumentMapper mapper;



    @Test(dataProvider = "fromDocumentToDBObject", dataProviderClass = MapperTestData.class)
    public void testMap(Document input, BasicDBObject output) {
        assertEquals(mapper.map(input), output);
    }

    @Test(dataProvider = "fromDBObjectToDocument", dataProviderClass = MapperTestData.class)
    public void testMap1(BasicDBObject input, Document output) {
        Document result = mapper.map(input);
        assertEquals(result.getMetadata().keySet(), output.getMetadata().keySet());
        assertEquals(result.getMetadata().values(), output.getMetadata().values());
        assertEquals(result.get_id(), output.get_id());
        assertEquals(result.getParent(), output.getParent());
    }
}