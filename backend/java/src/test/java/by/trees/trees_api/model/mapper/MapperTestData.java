package by.trees.trees_api.model.mapper;

import by.trees.trees_api.model.Document;
import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import org.testng.annotations.DataProvider;

import java.util.Collections;

public class MapperTestData {
    @DataProvider(name = "fromDocumentToDBObject")
    public static Object[][] documents() {
        return new Object[][]{
                {
                        new Document("testId", "testParent", Collections.emptyMap()),
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", Collections.emptyMap())
                },
                {
                        new Document("testId", null, Collections.emptyMap()),
                        new BasicDBObject("_id", "testId")
                                .append("parent", null)
                                .append("metadata", Collections.emptyMap())
                },
                {
                        new Document("testId", "testParent", ImmutableMap.of("name", "node name", "description", "node description")),
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", new BasicDBObject()
                                .append("name", "node name")
                                .append("description", "node description"))
                }
        };
    }

    @DataProvider(name = "fromDBObjectToDocument")
    public static Object[][] dbObjects() {
        return new Object[][]{
                {
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", Collections.emptyMap()),
                        new Document("testId", "testParent", Collections.emptyMap()),
                },
                {
                        new BasicDBObject("_id", "testId")
                                .append("parent", null)
                                .append("metadata", Collections.emptyMap()),
                        new Document("testId", null, Collections.emptyMap())
                },
                {
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", new BasicDBObject()
                                .append("name", "node name")
                                .append("description", "node description")),
                        new Document("testId", "testParent", ImmutableMap.of("name", "node name", "description", "node description")),
                }
        };
    }
}
