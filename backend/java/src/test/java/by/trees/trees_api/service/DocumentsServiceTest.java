package by.trees.trees_api.service;

import by.trees.trees_api.TreesApiApplication;
import by.trees.trees_api.model.Document;
import by.trees.trees_api.model.DocumentsDAO;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.*;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.*;

@TestExecutionListeners(MockitoTestExecutionListener.class)
@SpringBootTest
@ContextConfiguration(classes = TreesApiApplication.class)
public class DocumentsServiceTest extends AbstractTestNGSpringContextTests {

    @InjectMocks
    @Autowired
    private DocumentsService service;

    @MockBean
    private DocumentsDAO dao;



    @Test(dataProvider = "documentInsertProvider", dataProviderClass = DocumentsServiceTestData.class)
    public void testInsert(Document document, boolean result) {
        given(dao.insert(document)).willReturn(result);
        Optional<Document> insertResult = service.insert(document);
        if (result) {
            assertEquals(insertResult, Optional.of(document));
        } else {
            assertEquals(insertResult, Optional.empty());
        }
    }



    @Test(dataProvider = "deleteProvider", dataProviderClass = DocumentsServiceTestData.class)
    public void testDelete(String id, boolean result) {
        given(dao.delete(id)).willReturn(result);
        boolean deleteResult = service.delete(id);
        assertEquals(deleteResult, result);
    }



    @Test(dataProvider = "documentGetProvider", dataProviderClass = DocumentsServiceTestData.class)
    public void testGetAll(List<Document> documents) {
        System.out.println("1");
        given(dao.getAll()).willReturn(documents);
        List<Document> result = service.getAll();

        assertEquals(result, documents);


    }




    @Test(dataProvider = "updateProvider", dataProviderClass = DocumentsServiceTestData.class)
    public void testUpdate(String id, Document document, boolean result) {
        given(dao.update(id, document)).willReturn(result);
        Optional<Document> updateResult = service.update(id, document);
        if (result) {
            assertEquals(updateResult, Optional.of(document));
        } else {
            assertEquals(updateResult, Optional.empty());
        }
    }



    @Test(dataProvider = "updateMetaProvider", dataProviderClass = DocumentsServiceTestData.class)
    public void testUpdateMeta(String id, Map metadata, boolean result) {
        Document mockDocument = new Document(id, null, metadata);
        given(dao.updateMeta(id, metadata)).willReturn(result);
        given(dao.getById(id)).willReturn(mockDocument);
        Optional<Document> collection = service.updateMeta(id, metadata);
        if (result) {
            assertEquals(collection, Optional.of(mockDocument));
        } else {
            assertEquals(collection, Optional.empty());
        }
    }
}