package by.trees.trees_api.service;

import by.trees.trees_api.model.Document;
import com.google.common.collect.ImmutableMap;
import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.Collections;

public class DocumentsServiceTestData {
    @DataProvider(name = "documentInsertProvider")
    public static Object[][] documents() {
        return new Object[][]{
                {
                        new Document("nodeId", "parent", Collections.emptyMap()),
                        true
                },
                {
                        new Document(),
                        false
                }

        };
    }

    @DataProvider(name = "deleteProvider")
    public static Object[][] deleteProvider() {
        return new Object[][]{
                {"1", true},
                {"2", false }
        };
    }

    @DataProvider(name = "documentGetProvider")
    public static Object[][] collectionGet() {
        return new Object[][]{
                {
                        Arrays.asList(
                                new Document("1", "parent1", Collections.emptyMap()),
                                new Document("2", "parent2", Collections.emptyMap()),
                                new Document("3", "parent3", Collections.emptyMap())

                        )
                },
                {
                        Collections.emptyList()
                }
        };
    }

    @DataProvider(name = "updateProvider")
    public static Object[][] updateProvider() {
        return new Object[][]{
                {"1", new Document(), true},
                {"1", new Document(), false},
        };
    }

    @DataProvider(name = "updateMetaProvider")
    public static Object[][] updateMetaProvider() {
        return new Object[][]{
                {"1", ImmutableMap.of("name", "name1", "description", "description1"), true},
                {"1", ImmutableMap.of("name", "name1", "description", "description1"), false},
                {"1", Collections.emptyMap(), true},
                {"1", Collections.emptyMap(), false},
        };
    }
}
