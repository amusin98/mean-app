const documentsService = require('../service/documents');

module.exports.getAll = (req, res) => {
  return documentsService.getAll()
    .then(documents => sendJsonResponse(res, 200, documents))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Not found, cant get all documents',
    }));
};

module.exports.insert = (req, res) => {
  documentsService.insert({...req.body})
    .then(insertedDocument => sendJsonResponse(res, 201, insertedDocument))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Cant insert node',
    }));
};

module.exports.update = (req, res) => {
  if (!req.params.id) {
    sendJsonResponse(res, 404, {
      "message": 'Not found, document id is required',
    });
  }
  documentsService.update(req.params.id, {...req.body})
    .then(updatedDocument => sendJsonResponse(res, 200, updatedDocument))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Cant update node',
    }))
};

module.exports.updateMetadata = (req, res) => {
  if (!req.params.id) {
    sendJsonResponse(res, 404, {
      "message": 'Not found, id is required',
    });
  }
  documentsService.updateMetadata(req.params.id, req.body.metadata)
    .then(document => sendJsonResponse(res, 200, document))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Cant update node',
    }))
};

module.exports.delete = (req, res) => {
  if (!req.params.id) {
    sendJsonResponse(res, 404, {
      "message": 'Not found, id is required',
    });
  }
  documentsService.delete(req.params.id)
    .then(() => sendJsonResponse(res, 200, {}))
    .catch(() => sendJsonResponse(res, 400, {
      "message": 'Cant update node',
    }))
};

sendJsonResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};