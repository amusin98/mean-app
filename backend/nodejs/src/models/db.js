const mongoose = require('mongoose');

let dbUri = 'mongodb://localhost/collections_db';
mongoose.connect(dbUri);

if(process.env.NODE_ENV === 'production') {
  dbUri = 'mongodb://mikalai_amusin:AsDiS9820@ds241012.mlab.com:41012/collections_db';
}


const gracefulShutdown = (msg, callback) => {
  mongoose.connection.close(() => {
    console.log('Mongoose disconnected through: ' + msg);
    callback();
  });
};

process.once('SIGUSR2', () => {
  gracefulShutdown('nodemon restart', () => {
    process.kill(process.pid, 'SIGUSR2');
  });
});
process.on('SIGINT', () => {
  gracefulShutdown('app termination', () => {
    process.exit(0);
  });
});

mongoose.connection.on('connected', () => console.log('Connected to:' + dbUri));
mongoose.connection.on('error', error => console.log('connection error:' + error));
mongoose.connection.on('disconnected', () => console.log('disconnected'));

mongoose.model('Document', new mongoose.Schema({
  _id: {type: String, required: true},
  parent: String,
  metadata: mongoose.Schema.Types.Mixed
}));

