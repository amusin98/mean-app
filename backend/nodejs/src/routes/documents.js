const express = require('express');
const router = express.Router();
const documentsController = require('../controllers/documents');

router.get('/documents', documentsController.getAll);
router.post('/documents', documentsController.insert);
router.put('/documents/:id', documentsController.update);
router.put('/documents/:id/meta', documentsController.updateMetadata);
router.delete('/documents/:id', documentsController.delete);


module.exports = router;
