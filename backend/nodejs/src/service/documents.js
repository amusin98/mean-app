const mongoose = require('mongoose');
const documentsModel = mongoose.model('Document');

module.exports.getAll = () => {
  return documentsModel.find()
};

module.exports.getById = (id) => {
  return documentsModel.findOne({_id: id})
};

module.exports.insert = (document) => {
  return documentsModel.create(document)
};

module.exports.update = (nodeId, document) => {
  return new Promise((resolve, reject) => {
    documentsModel.deleteOne({_id: nodeId})
      .then(() => {
        documentsModel.create(document)
          .then(insertedDocument => {
            documentsModel.find({parent: nodeId})
              .then(documents => {
                Promise.all(documents.map(item => {
                  item.parent = insertedDocument._id;
                  return item.save()
                    .catch(e => Promise.reject(e));
                }))
                  .then(() => resolve(insertedDocument))
                  .catch(e => reject(e));
              })
          })
          .catch(e => reject(e));
      })
      .catch(e => reject(e));
  })
};

module.exports.updateMetadata = (id, metadata) => {
  return new Promise((resolve, reject) => {
    documentsModel.findOne({_id: id})
      .then(document => {
        document.metadata = metadata;
        document.save()
          .then(document => resolve(document))
          .catch(e => reject(e))
      })
      .catch(e => reject(e));
  })
};


const deleteRecursive = (id) => {
  return new Promise((resolve, reject) => {
    documentsModel.deleteOne({_id: id})
      .then(() => {
        documentsModel.find({parent: id})
          .then(documents => {
            Promise.all(documents.map(item => deleteRecursive(item._id)))
              .then(() => resolve())
              .catch(() => reject());
          })
          .catch(e => reject(e));
      })
      .catch(e => reject(e));
  })
};

module.exports.delete = (id) => {
  return deleteRecursive(id)
};