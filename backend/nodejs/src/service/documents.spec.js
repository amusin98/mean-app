const mongoose = require('mongoose');

describe('service tests', () => {
  const data = [
    {_id: 'nodeId', parent: null, metadata: {}},
    {_id: 'nodeId2', parent: null, metadata: {}},
    {_id: 'nodeId3', parent: null, metadata: {}},
  ];
  let service, model = {};
  beforeEach(() => {
    spyOn(mongoose, 'model').and.returnValue(model);
    service = require('./documents');
  });

  it('getAll test', () => {
    model.find = jasmine.createSpy('find').and.returnValue(new Promise((res, rej) => res(data)));
    service.getAll()
      .then((result) => {
        expect(result).toEqual(data);
        expect(model.find).toHaveBeenCalled()
      });
  });

  it('getById test success', () => {
    model.findOne = jasmine.createSpy('findOne').and.returnValue(new Promise((res, rej) => res(data[0])));
    service.getById('nodeId')
      .then((result) => {
        expect(result).toEqual(data[0]);
        expect(model.findOne).toHaveBeenCalledWith({_id: "nodeId"})
      });
  });


  it('getById test fail', () => {
    model.findOne = jasmine.createSpy('findOne').and.returnValue(new Promise((res, rej) => rej("Not found")));
    service.getById('nodeId')
      .catch(e => {
        expect(e).toEqual("Not found");
        expect(model.findOne).toHaveBeenCalledWith({_id: "nodeId"})
      });
  });

  it('insert test success', () => {
    model.create = jasmine.createSpy('create').and.returnValue(new Promise((res, rej) => res(data[0])));
    service.insert(data[0])
      .then((result) => {
        expect(result).toEqual(data[0]);
        expect(model.create).toHaveBeenCalledWith({...data[0]})
      });
  });

  it('insert test fail', () => {
    model.create = jasmine.createSpy('create').and.returnValue(new Promise((res, rej) => rej("Can't insert")));
    service.insert(data[0])
      .catch(e => {
        expect(e).toEqual("Can't insert");
        expect(model.create).toHaveBeenCalledWith({...data[0]})
      });
  });
  //
  it('update meta test success', () => {
    model.findOne = jasmine.createSpy('findOne').and.returnValue(new Promise((res, rej) => res(data[0])));
    data[0].save = jasmine.createSpy('save').and.returnValue(new Promise((res, rej) => res(data[0])));
    service.updateMetadata("nodeId", {})
      .then((result) => {
        expect(result).toEqual(data[0]);
        expect(model.findOne).toHaveBeenCalledWith({_id: 'nodeId'});
        expect(data[0].save).toHaveBeenCalled();
      });
  });

  it('update meta test fail', () => {
    model.findOne = jasmine.createSpy('findOne').and.returnValue(new Promise((res, rej) => rej('Not found')));
    service.updateMetadata("nodeId", {})
      .catch(e => {
        expect(e).toEqual('Not found');
        expect(model.findOne).toHaveBeenCalledWith({_id: 'nodeId'});
      });
  });
  it('delete test success', () => {
    model.deleteOne = jasmine.createSpy('deleteOne').and.returnValue(new Promise((res, rej) => res()));
    model.find = jasmine.createSpy('find').and.returnValue(new Promise((res, rej) => res([])));
    service.delete('nodeId')
      .then(() => {
        expect(model.deleteOne).toHaveBeenCalledWith({_id: 'nodeId'});
      });
  });

  it('delete test fail', () => {
    model.deleteOne = jasmine.createSpy('deleteOne').and.returnValue(new Promise((res, rej) => rej('Not found')));
    service.delete('nodeId')
      .catch(e => {
        expect(e).toEqual('Not found');
      });
  });

});