import angular from 'angular';
import component from './footer.component';

export default angular.module('footer', [])
  .component('footerComponent', component)
  .name;