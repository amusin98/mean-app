import template from './header.template.html';
import './header.less';

export default {
  bindings: {
    addDocumentCallback: '&',
    addNodeCallback: '&',
    deleteNodeCallback: '&',
    updateNodeCallback: '&',
    currentNode: '<',
  },
  template,
};
