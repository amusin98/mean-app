import angular from 'angular';
import component from './header.component';

export default angular.module('header', [])
  .component('headerComponent', component)
  .name;