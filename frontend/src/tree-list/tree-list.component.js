import template from './tree-list.template.html';
import './tree-list.less';

export default {
  template,
  bindings: {
    documents: '<',
    currentNode: '<',
    changeCurrentNodeCallback: '&',
    roots: '<'
  },
  controller: function treesController() {
    let vm = this;
    vm.childs = [];
    vm.childsShown = [];

    vm.$onChanges = function () {
      if (vm.documents && vm.roots) {
        vm.roots
          .forEach(document => vm.childs[document._id] = vm.documents.filter(item => item.parent === document._id));
      }
    };

    vm.haveChilds = (nodeId) => {
      return vm.childs[nodeId] && vm.childs[nodeId].length;
    };

    vm.showChilds = (nodeId) => {
      vm.childsShown[nodeId] = !vm.childsShown[nodeId];
    };

    vm.checkShowChilds = (nodeId) => {
      return !!vm.childsShown[nodeId];
    };

  },

};