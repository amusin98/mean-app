export default class TreesAPI {
  constructor($http) {
    this.client = $http;
  }

  getAll() {
    return this.client.get('/api/documents');
  }

  insert(node) {
    return this.client.post(`/api/documents`, JSON.stringify(node));
  }

  update(nodeId, newNode) {
    return this.client.put(`/api/documents/${nodeId}`, JSON.stringify(newNode));
  }

  updateMeta(nodeId, meta) {
    return this.client.put(`/api/documents/${nodeId}/meta`, meta);
  }

  delete(nodeId) {
    return this.client.delete(`/api/documents/${nodeId}`);
  }

}