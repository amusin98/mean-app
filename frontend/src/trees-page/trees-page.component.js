import template from './trees-page.template.html';
import './trees-page.less';
import toast from '../toast/toast-service';

function mainPartController(treesApi, ngToast) {
  let vm = this;
  let client = treesApi;
  client.getAll()
    .then(response => {
      vm.documents = response.data;
      vm.currentNode = vm.documents.find(document => document.parent === null);
      vm.roots = vm.documents.filter(document => document.parent === null);
    })
    .catch(e => toast.error(ngToast, e));

  vm.changeCurrentNodeCallback = function (nodeId) {
    vm.currentNode = vm.documents.find(document => document._id === nodeId);
  };

  vm.changeMetadataCallback = (metadata) => {
    client.updateMeta(vm.currentNode._id, {metadata: JSON.parse(metadata)})
      .then(() => vm.documents
        .find(document => document._id === this.currentNode._id).metadata = JSON.parse(metadata))
      .catch(e => toast.error(ngToast, e));
  };

  vm.addDocumentCallback = function (name) {
    client.insert({
      _id: name,
      parent: null,
      metadata: {}
    })
      .then((resp) => {
        vm.documents.push(resp.data);
        vm.roots.push(resp.data);
      })
      .catch(e => toast.error(ngToast, e));
  };

  vm.addNodeCallback = (name) => {
    client.insert({
      _id: name,
      parent: vm.currentNode._id,
      metadata: {}
    })
      .then(() => {
        vm.documents.push({_id: name, parent: vm.currentNode._id, metadata: {}});
        vm.roots = vm.documents.filter(document => document.parent === null);
      })
      .catch(e => toast.error(ngToast, e));
  };

  vm.updateNodeCallback = (name) => {
    client.update(vm.currentNode._id, {
      _id: name,
      parent: vm.currentNode.parent,
      metadata: vm.currentNode.metadata,
    })
      .then(() => {
        vm.documents
          .filter(document => document.parent === vm.currentNode._id)
          .map(document => document.parent = name);
        vm.documents.find(document => document._id === vm.currentNode._id)._id = name;
        vm.roots = vm.documents.filter(document => document.parent === null);
      })
      .catch(e => toast.error(ngToast, e));
  };

  vm.deleteNodeCallback = (node) => {
    client.delete(vm.currentNode._id)
      .then(() => {
        recursiveRemoving(node);
        vm.roots = vm.documents.filter(document => document.parent === null);
        vm.currentNode = vm.roots[0];
      })
      .catch(e => toast.error(ngToast, e));
  };

  const recursiveRemoving = (node) => {
    if (vm.documents.some(document => document.parent === node._id)) {
      let childs = vm.documents.filter(document => document.parent === node._id);
      vm.documents = vm.documents
        .filter(document => document.parent !== node._id && document._id !== node._id);
      childs.forEach(child => recursiveRemoving(child));
    }
    vm.documents = vm.documents.filter(document => document._id !== node._id);
  };
}

export default {
  template,
  controller: ['treesApi', 'ngToast', mainPartController],
};