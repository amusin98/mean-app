import angular from 'angular';
import component from './trees-page.component';
import trees from '../tree-list';
import nodeMetadata from '../node-metadata';
import headerModule from '../header';
import footerModule from '../footer';
import treesApi from './TreesAPI';


export default angular.module('treesPageModule', [
  headerModule,
  trees,
  nodeMetadata,
  footerModule,
  'ngToast',
])
  .component('treesPage', component)
  .service('treesApi', treesApi)
  .name;