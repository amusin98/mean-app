const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = () => {
  return {
    entry: './src/app.module.js'
    ,
    output: {
      filename: './app.bundle.js',
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
        },
        {test: /\.html$/, loader: "html-loader"},
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.less/,
          use: ['style-loader', 'css-loader', 'less-loader'],
        },
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './public/index.html',
        filename: './index.html',
      }),
      new webpack.HotModuleReplacementPlugin(),
    ],
    devtool: "#inline-source-map",
    devServer: {
      hot: true,
      port: 3000,
      historyApiFallback: true,
      proxy: {
        //8090 - java, 9090 - nodejs
        '/api': 'http://localhost:8090',
      },
    },
  };
};